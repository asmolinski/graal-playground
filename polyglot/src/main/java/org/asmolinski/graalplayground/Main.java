package org.asmolinski.graalplayground;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;

import java.io.InputStreamReader;
import java.io.Reader;

public class Main {

  public static void main(String[] args) {
    System.out.println(getRandomAddressFromJs());
  }

  private static String getRandomAddressFromJs() {
    try (Context polyglot = Context.create()) {
      Reader streamReader = new InputStreamReader(Main.class.getResourceAsStream("/chance.js"));
      Source source = Source.newBuilder("js", streamReader, "chance.js").build();
      polyglot.eval("js", "var exports = {}");
      polyglot.eval(source);
      return polyglot.eval("js", "new exports.Chance().address()").asString();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
