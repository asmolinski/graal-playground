function run() {
    // first 2 args are "node" and path to entrypoint script
    const dataSize = process.argv[2];
    const iterations = process.argv[3];
    const warmupIterations = Math.min(process.argv[4] || 0, iterations);
    runTest(dataSize, iterations, warmupIterations);
}

function runTest(dataSize, iterations, warmupIterations) {
    let counter = 0;
    if (warmupIterations > 0) {
        console.log("Warming up...");
        while (counter < warmupIterations) {
            console.log("Warmup iteration " + (counter + 1));
            sortDoubles(dataSize);
            counter++;
        }
        console.log("Warmup completed.");
    }
    const startTime = new Date().getTime();
    console.log("Running test...");
    while (counter < iterations) {
        console.log("Test iteration " + (counter - warmupIterations + 1));
        sortDoubles(dataSize);
        counter++;
    }
    const stopTime = new Date().getTime();
    console.log("Test completed. Time elapsed " + (stopTime - startTime) + " ms");
}

function sortDoubles(dataSize) {
    let arr = Array.from({length: dataSize}, () => Math.random());
    arr.sort();
}

run();
