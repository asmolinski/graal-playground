package org.asmolinski.graalplayground;

import java.util.Random;

import static java.util.stream.Collectors.toList;

class Main {

  private final Random random = new Random();

  public static void main(String[] args) {
    final int dataSize = Integer.parseInt(args[0]);
    final int iterations = Integer.parseInt(args[1]);
    final int warmupIterations = args.length > 2
        ? Math.min(Integer.parseInt(args[2]), iterations)
        : 0;
    new Main().runTest(dataSize, iterations, warmupIterations);
  }

  private void runTest(final int dataSize, final int iterations, final int warmupIterations) {
    int counter = 0;
    if (warmupIterations > 0) {
      System.out.println("Warming up...");
      while (counter < warmupIterations) {
        System.out.println("Warmup iteration " + (counter + 1));
        sortDoubles(dataSize);
        counter++;
      }
      System.out.println("Warmup completed.");
    }
    final long startTime = System.currentTimeMillis();
    System.out.println("Running test...");
    while (counter < iterations) {
      System.out.println("Test iteration " + (counter - warmupIterations + 1));
      sortDoubles(dataSize);
      counter++;
    }
    final long stopTime = System.currentTimeMillis();
    System.out.println("Test completed. Time elapsed " + (stopTime - startTime) + " ms");
  }

  private void sortDoubles(final int dataSize) {
    random.doubles(dataSize).boxed().sorted().collect(toList());
  }
}
